import axios from "axios";
import * as r from "react";

const useCreateParty = () => {
    const [loading, setLoading] = r.useState(false);
    const [error, setError] = r.useState([]);

    const party = async (partyData) => {
        setLoading(true);
        setError(null);

        try {
            console.log(partyData, "party data");
            const data = await axios.post("http://localhost:8080/api/parties", partyData);
            setLoading(false);
            return data;
        }
        catch (e) {
            setError(e.message);
            setLoading(false);
            throw e;
        }
    };

    return {error, loading, party};
};
export default useCreateParty;
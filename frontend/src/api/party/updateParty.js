import axios from "axios";
import * as r from "react";

const useUpdateParty = () => {
    const [loading, setLoading] = r.useState(false);
    const [error, setError] = r.useState([]);

    const updateParty = async(id, partyData) =>  {
        setLoading(true);
        setError(null);

        try {
            console.log(partyData, "party data");
            const data = await axios.put(`http://localhost:8080/${id}`, partyData);
            setLoading(false);
            return data;
        }
        catch (e) {
            setError(e.message);
            setLoading(false);
            throw e;
        }
    };
    return { updateParty, loading, error};
}
export default useUpdateParty;
import { useState } from "react";
import axios from 'axios';

export default function useDeleteParty() {
    const [loadingParty, setLoadingParty] = useState(true);
    const [error, setErrorParty] = useState([]);
    const removeData = async (id) => {
        setLoadingParty(true);
        try {
            const response = await axios.delete(`http://localhost:8080/delete/${id}`);
            console.log("response", response.data);
            setLoadingParty(false);
            return response.data;
        }
        catch (error) {
            setLoadingParty(false);
            setErrorParty(error);
            throw error;
        }
    };
    return { removeData, loadingParty, error };
}


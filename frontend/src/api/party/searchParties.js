import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';

const useParties = () => {
    const [data, setData] = useState(null); // Initialize as null to handle empty states better
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchParties = useCallback(async (location, type, page, size) => {
        setLoading(true);
        setError(null); // Reset error state before new request
        try {
            const response = await axios.get('http://localhost:8080/api/parties', {
                params: { location, type, page, size }
            });
            console.log(response, 'search');
            setData(response.data);
        } catch (err) {
            setError(err.message);
        } finally {
            setLoading(false);
        }
    }, []);

    useEffect(() => {

    }, [fetchParties]);

    return { data, loading, error, fetchParties };
};

export default useParties;

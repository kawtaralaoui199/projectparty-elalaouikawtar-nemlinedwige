import * as r from "react";
import axios from "axios";

const useParties = () => {
    const [parties, setParties] = r.useState([]);
    const [loading, setLoading] = r.useState([]);
    const [error, setError] = r.useState(null);

    r.useEffect(() => {
        fetchParties();
    }, []);
        const fetchParties = async () => {
            try {
                const response = await axios.get("http://localhost:8080/all");
                console.log(response.data, 'results');
                setParties(response.data);
                setLoading(false);
            }
            catch (e) {
                setError(error);
                setLoading(false);
            }
        };
    fetchParties();

    return { parties, loading, error};
};

export default useParties;
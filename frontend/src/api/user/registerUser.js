import axios from "axios";
import { useState } from "react";

const useRegistration = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const register = async (userData) => {
        setLoading(true);
        setError(null);
        try {
            console.log(userData, "form values");
            const response = await axios.post('http://localhost:8080/register', userData);
            setLoading(false);
            return response;
        } catch (err) {
            setError(err.message);
            setLoading(false);
            throw err;
        }
    };

    return { error, loading, register };
};

export default useRegistration;

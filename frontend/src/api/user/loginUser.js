import axios from 'axios';
import {useState} from "react";

const useLogin = () => {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const login = async (userData) => {
        setLoading(true);
        setError(null);
        try {
            console.log(userData, "form values");
            const response = await axios.post('http://localhost:8080/login', null, {
                params : {
                    email: userData.email,
                    password: userData.password
                }
            });
            setLoading(false);
            return response;
        } catch (err) {
            setError(err.message);
            setLoading(false);
            throw err;
        }
    };

    return { error, loading, login};
};

export default useLogin;
import Form from 'react-bootstrap/Form';
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import useLogin from "../api/user/loginUser";
import * as r from "react";
import Alert from "react-bootstrap/Alert";

function LoginForm() {
    const {error, loading, login} = useLogin();
    const [userData, setUserdata] = r.useState({
        email: '',
        password: ''
    });
    const handleChange = (e) => {
        const { name, value } = e.target;
        setUserdata((prevData) => ({ ...prevData, [name]: value }));
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await login(userData);
        } catch (err) {
            console.error("Registration failed", err);
        }
    };

    if (error) return <Alert variant="danger"> Login failed </Alert>;

    return (
        <div className="container" style={{display: 'flex', justifyContent: 'space-evenly'}}>
            <div className="col-6" style={{marginTop: '15em', color: "#FFFFFF"}}>
                <h1> Connexion </h1>
                <p> Retrouvez l'integralité de vos soirée dans votre espace membre ! </p>
            </div>
            <div className="col-6">
            <Card className="container" style={{marginTop: '10em'}}>
        <Form style={{marginTop: "1em"}} onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Adresse e-mail </Form.Label>
                <Form.Control type="email" placeholder="name@example.com" name="email" value={userData.email}
                              onChange={handleChange}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlPassword">
                <Form.Label>Mot de passe</Form.Label>
                <Form.Control type="password" name="password" value={userData.password}
                              onChange={handleChange}/>
            </Form.Group>
            <Button type="submit" className="w-100" style={{marginBottom: '2em'}}> Se connecter</Button>
        </Form>
            </Card>
            </div>
        </div>
    );
}

export default LoginForm;
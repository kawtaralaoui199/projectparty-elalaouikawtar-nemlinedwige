import Form from 'react-bootstrap/Form';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import useRegistration from "../api/user/registerUser";
import Alert from "react-bootstrap/Alert";
import * as r from 'react';
import {FormGroup} from "react-bootstrap";

const RegisterForm = () => {
    const {error, loading, register} = useRegistration();
    const [userData, setUserdata] = r.useState({
        email: '',
        password: '',
        age: '',
        region: '',
        city: '', interests: ''
    });

    const handleChange = (e) => {
        const {name, value} = e.target;
        setUserdata((prevData) => ({
            ...prevData,
            [name]: name === 'age' ? parseInt(value, 10) : value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await register(userData);
        } catch (err) {
            console.error("Registration failed", err);

        }
    };

    if (error) return <Alert variant="danger"> Registration failed </Alert>;

    return (
        <div>
            <Card className="container col-4" style={{marginTop: '5em'}}>
                <Form style={{marginTop: "1em"}} onSubmit={handleSubmit}>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Adresse e-mail </Form.Label>
                        <Form.Control type="email" placeholder="name@example.com" name="email" value={userData.email}
                                      onChange={handleChange}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlPassword">
                        <Form.Label>Mot de passe</Form.Label>
                        <Form.Control type="password" value={userData.password} name="password"
                                      onChange={handleChange}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlAge">
                        <Form.Label>Age</Form.Label>
                        <Form.Control type="number" value={userData.age} name="age" onChange={handleChange}/>
                    </Form.Group>
                    <Form.Select className="mb-3" aria-label="Default select example" name="region"
                                 value={userData.region} onChange={handleChange}>
                        <option>Region</option>
                        <option value="Ile-de-France">Ile de France</option>
                        <option value="Floride">Floride</option>
                        <option value="Nouvelle Aquitaine">Nouvelle Aquitaine</option>
                    </Form.Select>

                    <Form.Select className="mb-3" aria-label="Default select example" name="city" value={userData.city}
                                 onChange={handleChange}>
                        <option>Ville</option>
                        <option value="Paris">Paris</option>
                        <option value="Milan">Milan</option>
                        <option value="Tokyo">Tokyo</option>
                    </Form.Select>
                    <Form.Label>Centres d'interets</Form.Label>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlEmail">
                        <Form.Control as="textarea" rows={3} name="interests" value={userData.interests}
                                      onChange={handleChange}/>
                    </Form.Group>
                    <Button type="submit" className="w-100" style={{marginBottom: '2em'}}
                            disabled={loading}> Inscription</Button>
                </Form>
            </Card>
        </div>
    );
}

export default RegisterForm;
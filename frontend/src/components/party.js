import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import useParties from "../api/party/getParties";
import Badge from "react-bootstrap/Badge";
import useDeleteParty from "../api/party/deleteParty";

function PartyCard() {
    const {parties, loading, error} = useParties();
    const {removeData } = useDeleteParty();

    if (loading) return <p> ... loading</p>;
    if (error) return <p> error : {error.message}</p>;

    const handleDelete = async (id) => {
        try {
            await removeData(id);
        } catch (error) {
            return 'Failed to delete the team.';
        }
    };

    return (
        <div>
            <h1 className="container" style={{marginTop: '2rem'}}> Toutes les soirées </h1>

            <div className="container" style={{marginTop: '3rem', display: "flex", justifyContent: "space-evenly"}}>
                {parties.map((party) => (
                    <Card style={{width: '22rem'}} key={party.id}>
                        <Card.Body>
                            <Card.Title>{party.name} - {party.location} </Card.Title>
                            <Card.Subtitle> <Badge bg="danger">{party.type}</Badge> | {party.dateTime}</Card.Subtitle>
                            <Card.Text>
                                Place restantes : {party.remainingSpots}
                                <br/>
                                Type : {party.type}
                                Prix : {party.price} €
                            </Card.Text>
                            <Button variant="primary" href={`/updateParty/${party.id}`}>Update </Button>
                            <Button variant="danger" onClick={() => handleDelete(party.id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     className="bi bi-trash3" viewBox="0 0 16 16">
                                    <path
                                        d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5"/>
                                </svg>
                            </Button>
                        </Card.Body>
                    </Card>
                ))}
            </div>
        </div>
    );
}

export default PartyCard;
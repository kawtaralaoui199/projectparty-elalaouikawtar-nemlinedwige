import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";

const AppNavbar = () => {
    return (
        <Navbar fixed="top" bg="light" data-bs-theme="light">
            <Container>
                <Navbar.Brand href="#home"></Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link href="/">Toutes les soirées </Nav.Link>
                    <Nav.Link href="/createParty">Créer une soirée </Nav.Link>
                </Nav>
                <Nav>
                    <Nav.Link href="/login"> Connexion</Nav.Link>
                    <Button className="app-button" href="/register">Inscription</Button>
                </Nav>
            </Container>
        </Navbar>
    )
}
export default AppNavbar;
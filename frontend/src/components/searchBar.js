import React, { useState } from 'react';
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import useParties from "../api/party/searchParties";
import ListGroup from 'react-bootstrap/ListGroup';
import Spinner from 'react-bootstrap/Spinner';

const SearchBar = () => {
    const [location, setLocation] = useState('');
    const [type, setType] = useState('');
    const [page, setPage] = useState(0);
    const [size, setSize] = useState(10);

    const { data, loading, error, fetchParties } = useParties();

    const handleSearch = (e) => {
        e.preventDefault();
        fetchParties(location, type, page, size);
    };

    return (
        <div className="searchBck">
        <div className="container col-6" style={{ paddingTop: "10em", paddingBottom: "5em" }}>
            <div className="col-4">
                <h1 style={{color: "#FFFFFF"}}>Rechercher une soirée</h1>
                <p style={{color: "#FFFFFF"}}>Merci de remplir les champs suivants afin de trouver une soirée</p>
            </div>
            <Form onSubmit={handleSearch}>
                <Row>
                    <Col>
                        <Form.Control
                            type="text"
                            placeholder="Location"
                            value={location}
                            onChange={(e) => setLocation(e.target.value)}
                        />
                    </Col>
                    <Col>
                        <Form.Control
                            type="text"
                            placeholder="Type"
                            value={type}
                            onChange={(e) => setType(e.target.value)}
                        />
                    </Col>

                    <Col xs="auto">
                        <Button type="submit">Search</Button>
                    </Col>
                </Row>
            </Form>
            {loading && <Spinner animation="border" />}
            {error && <p>Error: {error}</p>}
            <ListGroup>
                {data?.content && data.content.map((party) => (
                    <ListGroup.Item key={party.id}>
                        <strong>{party.name}</strong><br />
                        Location: {party.location}<br />
                        Type: {party.type}<br />
                        Date: {new Date(party.date_time).toLocaleString()}<br />
                        Remaining Spots: {party.remaining_spots}<br />
                        Price: {party.is_paid ? `$${party.price}` : 'Free'}
                    </ListGroup.Item>
                ))}
            </ListGroup>
        </div>
        </div>
    );
};

export default SearchBar;

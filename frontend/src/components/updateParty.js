import Form from "react-bootstrap/Form";
import useUpdateParty from "../api/party/updateParty";
import * as r from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { useParams } from 'react-router-dom';

const PartyForm = () => {
    const { id } = useParams();
    const {error, loading, updateParty} = useUpdateParty();
    const [partyData, setPartyData] = r.useState({
        name: '',
        location: '',
        type: '',
        datetime: '',
        remainingSpots: '',
        price: '',
        email: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setPartyData((prevData) => ({
            ...prevData,
            [name]: name === 'age' ? parseInt(value, 10) : value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await updateParty(id, partyData);
        } catch (err) {
            console.error("Party update failed", err);

        }
    };

    return (
        <div className="container" style={{display: 'flex', justifyContent: 'space-evenly'}}>
            <div className="col-6" style={{marginTop: '15em', color: "#FFFFFF"}}>
                <h1> Créer votre propre soirée </h1>
                <p> VUn changement de dernière minute? Pas de soucis ! Modifie autant de fois que tu veux ta soirée !</p>
                <p> PS: Completez tous les champs permet d'attirer un max d'invités ! Enjoy ;) </p>

            </div>
            <div className="col-6">
                <Card className="container " style={{marginTop: '5em'}}>
                    <Form style={{marginTop: "1em"}} onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlName">
                            <Form.Label> Nom de la soirée </Form.Label>
                            <Form.Control type="text" placeholder="Soirée Jeux vidéos" name="name" value={partyData.name}
                                          onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlLocation">
                            <Form.Label>Lieu</Form.Label>
                            <Form.Control type="text" value={partyData.location} name="location"
                                          onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlType">
                            <Form.Label>Type </Form.Label>
                            <Form.Control type="text" value={partyData.type} name="type" onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlDate">
                            <Form.Label>Date </Form.Label>
                            <Form.Control type="date" value={partyData.dateTime} name="datetime" onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlRemainingSpots">
                            <Form.Label>Places restantes</Form.Label>
                            <Form.Control type="number" value={partyData.remainingSpots} name="remainingSpots" onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlPrice">
                            <Form.Label>Prix </Form.Label>
                            <Form.Control type="number" value={partyData.price} name="price" onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" value={partyData.email} name="email" onChange={handleChange}/>
                        </Form.Group>
                        <Button type="submit" className="w-100" style={{marginBottom: '2em'}}
                                disabled={loading}> Mettre à jour la soirée la soirée </Button>
                    </Form>
                </Card>
            </div>
        </div>
    );
}
export default PartyForm;
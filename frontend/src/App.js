import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import IndexPage from "./pages/indexPage";
import LoginPage from "./pages/loginPage";
import RegisterPage from "./pages/registerPage";
import SearchPage from "./pages/searchPage";
import CreatePartyPage from "./pages/createPartyPage";
import UpdatePartyPage from "./pages/updateParty";
import "@fontsource/poppins";

function App() {
  return (
      <BrowserRouter>
          <Routes>
              <Route path="/" element={<IndexPage/>} />
              <Route path="/login" element={<LoginPage/>}/>
              <Route path="/register" element={<RegisterPage/>}/>
              <Route path="/searchParties" element={<SearchPage/>}/>
              <Route path="/createParty" element={<CreatePartyPage/>}/>
              <Route path="/updateParty/:id" element={<UpdatePartyPage/>}/>
          </Routes>
      </BrowserRouter>
  );
}

export default App;

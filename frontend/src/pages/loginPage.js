import AppNavbar from "../components/menu";
import LoginForm from "../components/loginForm";

const LoginPage = () => {
    return (
        <div className="loginBck">
            <AppNavbar/>
            <LoginForm/>
        </div>

    )
}
export default LoginPage
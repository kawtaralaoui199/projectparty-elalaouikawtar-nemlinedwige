import AppNavbar from "../components/menu";
import PartyForm from "../components/partyForm";

const CreatePartyPage = () => {
    return (
        <body>
        <div className="createPartyPage">
            <AppNavbar/>
            <PartyForm/>
        </div>
        </body>

    )
}
export default CreatePartyPage;
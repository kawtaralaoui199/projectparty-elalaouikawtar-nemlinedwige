import React from "react";
import MenuApp from "../components/menu";
import PartyCard from "../components/party";
import Button  from "react-bootstrap/Button";
import SearchBar from "../components/searchBar";

const IndexPage = () => {
    return (
        <body>
        <div>
            <MenuApp/>
            <SearchBar/>
            <PartyCard/>
        </div>
        </body>

    );
}

export default IndexPage;
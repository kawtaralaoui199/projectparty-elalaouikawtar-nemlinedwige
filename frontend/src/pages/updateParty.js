import AppNavbar from "../components/menu";
import PartyForm from "../components/updateParty";

const UpdatePartyPage = () => {
    return (
        <body>
        <div className="updateParty">
            <AppNavbar/>
            <PartyForm/>
        </div>
        </body>

    )
}
export default UpdatePartyPage;
import SearchBar from "../components/searchBar";
import AppNavbar from "../components/menu";

const SearchPage = () => {
    return (<body>
        <div className="searchBck">
            <AppNavbar/>
            <SearchBar/>
        </div>
        </body>

    )
}
export default SearchPage;
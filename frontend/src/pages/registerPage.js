import AppNavbar from "../components/menu";
import RegisterForm from "../components/registerForm";

const RegisterPage = () => {
    return (
        <body>
        <div>
            <AppNavbar/>
            <RegisterForm/>
        </div>
        </body>

    );
}
export default RegisterPage;
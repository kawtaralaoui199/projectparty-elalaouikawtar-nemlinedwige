### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
### Fonctionnalités

Mise en cache : Amélioration des performances grâce à des mécanismes de mise en cache.
Batch Fetching : Optimisation des accès à la base de données avec le batch fetching d'Hibernate.
Optimisation des index : Ajout d'index sur les bases de données pour des performances de requêtes plus rapides.
Intégration front-end : Intégration fluide avec une application front-end en React pour une expérience utilisateur optimale.

### Technologies Utilisées
Spring Boot
Spring Data JPA
Hibernate
Jakarta Persistence (JPA)
Lombok
Maven
React

### Implémentations Clés
### Mise en cache
Implémentation de la mise en cache pour améliorer les performances en stockant les données fréquemment accédées en mémoire.

Utilisation des annotations @Cacheable, @CachePut et @CacheEvict pour gérer la mise en cache.
Configuration de la mise en cache dans les classes UserService et PartyService.
### Batch Fetching
Optimisation des accès à la base de données avec le batch fetching pour réduire le nombre de requêtes SQL.

Utilisation de l'annotation @BatchSize(size = 10) sur les collections dans les entités Party et User.
### Optimisation des index
Ajout d'index aux tables de la base de données pour améliorer les performances des requêtes.

Ajout des annotations @Index dans les entités Party et User pour les champs fréquemment requis.
### Intégration front-end
Intégration avec une application front-end en React pour fournir une expérience utilisateur fluide.

Les endpoints sont conçus pour supporter les opérations front-end telles que la création, la mise à jour et la recherche de fêtes et d'utilisateurs.
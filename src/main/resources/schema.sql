
-- Create users table
CREATE TABLE users (
                       id BIGINT AUTO_INCREMENT PRIMARY KEY,
                       email VARCHAR(255) UNIQUE NOT NULL,
                       password VARCHAR(255) NOT NULL,
                       region VARCHAR(255),
                       city VARCHAR(255),
                       age INT,
                       interests VARCHAR(255)
);
CREATE TABLE parties (
                         id BIGINT AUTO_INCREMENT PRIMARY KEY,
                         name VARCHAR(255),
                         location VARCHAR(255),
                         type VARCHAR(255),
                         date_time TIMESTAMP,
                         remaining_spots INT,
                         is_paid BOOLEAN,
                         price DOUBLE,
                         organizer_id BIGINT,
                         CONSTRAINT fk_organizer FOREIGN KEY (organizer_id) REFERENCES users(id)
);

-- Create index on parties table
CREATE INDEX idx_party_location_type ON parties (location, type);

-- Create join table for many-to-many relationship
CREATE TABLE user_party_attendance (
                                       user_id BIGINT,
                                       party_id BIGINT,
                                       CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id),
                                       CONSTRAINT fk_party FOREIGN KEY (party_id) REFERENCES parties(id),
                                       PRIMARY KEY (user_id, party_id)
);

-- Create index on users table
CREATE INDEX idx_user_email ON users (email);

-- Insert users
INSERT INTO users (email, password, region, city, age, interests) VALUES
                                                                          ('john.doe@example.com', 'password123', 'North', 'New York', 30, 'Hiking, Reading'),
                                                                          ('jane.smith@example.com', 'password456', 'East', 'Boston', 25, 'Music, Painting'),
                                                                          ('alice.jones@example.com', 'password789', 'West', 'San Francisco', 35, 'Traveling, Cooking');

-- Insert parties
INSERT INTO parties (name, location, type, date_time, remaining_spots, is_paid, price, organizer_id) VALUES
                                                                                                             ('Beach Party', 'Miami Beach', 'Outdoor', '2024-07-15T18:00:00', 50, true, 25.0, 1),
                                                                                                             ('Music Concert', 'Central Park', 'Concert', '2024-08-20T19:00:00', 100, false, 0.0, 2),
                                                                                                             ('Cooking Workshop', 'Downtown Kitchen', 'Workshop', '2024-09-05T14:00:00', 20, true, 50.0, 3);

-- Insert relationships (attendees)
-- User 1 attending Party 1 and 2
INSERT INTO user_party_attendance (user_id, party_id) VALUES (1, 1);
INSERT INTO user_party_attendance (user_id, party_id) VALUES (1, 2);

-- User 2 attending Party 2 and 3
INSERT INTO user_party_attendance (user_id, party_id) VALUES (2, 2);
INSERT INTO user_party_attendance (user_id, party_id) VALUES (2, 3);

-- User 3 attending Party 1
INSERT INTO user_party_attendance (user_id, party_id) VALUES (3, 1);

package com.example.demo.Repository;


import com.example.demo.entity.Party;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PartyRepository extends JpaRepository<Party, Long> {
    @Query("SELECT p FROM Party p WHERE p.location = :location AND p.type = :type")
    Page<Party> findByLocationAndType(@Param("location") String location, @Param("type") String type, Pageable pageable);
}

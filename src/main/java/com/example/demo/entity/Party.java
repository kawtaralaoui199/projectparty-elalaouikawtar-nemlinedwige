package com.example.demo.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.BatchSize;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Entity
@Table(name = "parties", indexes = {
        @Index(name = "idx_party_location_type", columnList = "location, type")
})
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Party {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private String name;
    private String location;
    private String type;
    private LocalDateTime dateTime;
    private int remainingSpots;
    private boolean isPaid;
    private double price;

    @ManyToOne
    @JoinColumn(name = "organizer_id")
    @JsonManagedReference
    private User organizer;

    @ManyToMany(mappedBy = "attendedParties")
    @BatchSize(size = 10)
    @JsonManagedReference
    private Set<User> attendees;
}

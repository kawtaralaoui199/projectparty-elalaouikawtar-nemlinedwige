package com.example.demo.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.BatchSize;
import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users", indexes = {
        @Index(name = "idx_user_email", columnList = "email")
})
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(unique = true)
    private String email;

    private String password;
    private String region;
    private String city;
    private int age;
    private String interests;

    @OneToMany(mappedBy = "organizer")
    @BatchSize(size = 10)
    @JsonBackReference
    private Set<Party> organizedParties;

    @ManyToMany
    @JoinTable(
            name = "user_party_attendance",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "party_id")
    )
    @BatchSize(size = 10)
    @JsonBackReference
    private Set<Party> attendedParties;
}

package com.example.demo.service;

import com.example.demo.Repository.PartyRepository;
import com.example.demo.Repository.UserRepository;
import com.example.demo.dto.PartyDto;
import com.example.demo.entity.Party;
import com.example.demo.entity.User;
import com.example.demo.mapper.PartyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PartyService {

    @Autowired
    private PartyRepository partyRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PartyMapper partyMapper;
    private HttpSession session;

    @Cacheable("all")
    public Page<PartyDto> getAll(Pageable pageable) {
        Page<Party> parties = partyRepository.findAll(pageable);
        return parties.map(partyMapper::convertToDTO);
    }

    @Cacheable(value = "parties", key = "#location + '-' + #type")
    public Page<Party> findParties(String location, String type, Pageable pageable) {
        return partyRepository.findByLocationAndType(location, type, pageable);
    }

    @Caching(
            put = { @CachePut(value = "parties", key = "#result.id") },
            evict = { @CacheEvict(value = "all", allEntries = true) }
    )
    public Party createParty(PartyDto party) {
        var partyCreated = partyMapper.dtoToEntity(party);
        return partyRepository.save(partyCreated);
    }

    @Caching(
            put = { @CachePut(value = "parties", key = "#result.id") },
            evict = { @CacheEvict(value = "all", allEntries = true) }
    )
    public Party updateParty(PartyDto party) {
        var partyUpdated = partyMapper.dtoToEntity(party);
        return partyRepository.save(partyUpdated);
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "parties", key = "#id"),
                    @CacheEvict(value = "all", allEntries = true)
            }
    )
        @Transactional
        public void deleteParty(Long id) {
            Optional<Party> partyOpt = partyRepository.findById(id);
            if (partyOpt.isPresent()) {
                Party party = partyOpt.get();
                party.getAttendees().forEach(user -> {
                    user.getAttendedParties().remove(party);
                    userRepository.save(user);
                });
                partyRepository.deleteById(id);
            } else {
                throw new IllegalArgumentException("Party not found with id " + id);
            }
        }    }


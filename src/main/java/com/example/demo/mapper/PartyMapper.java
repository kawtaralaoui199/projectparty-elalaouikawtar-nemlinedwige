package com.example.demo.mapper;

import com.example.demo.Repository.UserRepository;
import com.example.demo.dto.PartyDto;
import com.example.demo.entity.Party;
import com.example.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PartyMapper {

    @Autowired
    private UserRepository userRepository;

    public PartyDto convertToDTO(Party party) {
        PartyDto dto = new PartyDto();
        dto.setId(party.getId());
        dto.setName(party.getName());
        dto.setLocation(party.getLocation());
        dto.setType(party.getType());
        dto.setDateTime(party.getDateTime());
        dto.setRemainingSpots(party.getRemainingSpots());
        dto.setPaid(party.isPaid());
        dto.setPrice(party.getPrice());
        dto.setOrganizerEmail(party.getOrganizer().getEmail());
        dto.setAttendeeEmails(party.getAttendees().stream().map(User::getEmail).collect(Collectors.toSet()));
        return dto;
    }

    public Party dtoToEntity(PartyDto dto) {
        Party party = new Party();
        // Ensure id is not set to let the database handle it
        party.setId(dto.getId());
        party.setName(dto.getName());
        party.setLocation(dto.getLocation());
        party.setType(dto.getType());
        party.setDateTime(dto.getDateTime());
        party.setRemainingSpots(dto.getRemainingSpots());
        party.setPaid(dto.isPaid());
        party.setPrice(dto.getPrice());

        // Fetch organizer by email
        User organizer = userRepository.findByEmail(dto.getOrganizerEmail())
                .orElseThrow(() -> new IllegalArgumentException("Organizer with email " + dto.getOrganizerEmail() + " not found"));
        party.setOrganizer(organizer);

        // Fetch attendees by emails
        Set<User> attendees = dto.getAttendeeEmails().stream()
                .map(email -> userRepository.findByEmail(email)
                        .orElseThrow(() -> new IllegalArgumentException("Attendee with email " + email + " not found")))
                .collect(Collectors.toSet());
        party.setAttendees(attendees);

        return party;
    }
}

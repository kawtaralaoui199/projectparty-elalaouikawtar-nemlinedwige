package com.example.demo.controller;

import com.example.demo.dto.PartyDto;
import com.example.demo.entity.Party;
import com.example.demo.service.PartyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class PartyController {
    @Autowired
    private PartyService partyService;

    @GetMapping("/api/parties")
    public List<Party> searchParties(@RequestParam String location, @RequestParam String type,
                                     @RequestParam int page, @RequestParam int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Party> partyPage = partyService.findParties(location, type, pageable);
        return partyPage.getContent();
    }

    @GetMapping("/all")
    public List<PartyDto> getAllParties(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<PartyDto> partyPage = partyService.getAll(pageable);
        return partyPage.getContent();
    }

    @PostMapping("/api/parties")
    public ResponseEntity<Party> createParty(@Validated @RequestBody PartyDto partyDto) {
        Party createdParty = partyService.createParty(partyDto);
        return new ResponseEntity<>(createdParty, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public Party updateParty(@PathVariable Long id, @RequestBody PartyDto partyDto) {
        partyDto.setId(id);
        return partyService.updateParty(partyDto);
    }

    @DeleteMapping("delete/{id}")
    public void deleteParty(@PathVariable Long id) {
        partyService.deleteParty(id);
    }
}

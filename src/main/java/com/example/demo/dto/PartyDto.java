package com.example.demo.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;
@Data
@NoArgsConstructor
public class PartyDto {

    private Long id;
    private String name;
    private String location;
    private String type;
    private LocalDateTime dateTime;
    private int remainingSpots;
    private boolean isPaid;
    private double price;
    private String organizerEmail;
    private Set<String> attendeeEmails;
}

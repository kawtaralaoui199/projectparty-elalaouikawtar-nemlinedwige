package com.example.demo.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
@Data
@NoArgsConstructor
public class UserDto {

    private Long id;
    private String email;
    private String region;
    private String city;
    private int age;
    private String interests;
    private Set<String> organizedPartyNames;
    private Set<String> attendedPartyNames;

}